# DISTRIBUCIÓ DE FEINA #

### Submit / ask ###

* Página Submit : All team
* Submit post : Marc 
* Submit ask : All team
* Like post : Oriol 
* Dislike post : Oriol 
* Validate Url : Sergi 

### Comentaris ###

* Página comentaris : All team
* Comentaris : Oriol - Sergi
* Reply Commenatris : Marc
* Like comentaris: All team
* Dislike comentaris: All team
* Pagina Replay: Adri - Edu

### Usuari ###

* Registre/Login : Marc 
* Página Usuari : Marc 
* Página Favoritos: Edu
* Favoritos: Adri
* Comments : Edu
* Submisions : Adri

### Problemes ###

En la situació actual que estem ens ha sigut dificil quedar amb tots els membres del grup i poder treballar tots junts, pero a través de videoconferencies i el drive hem pogut resoldre aquesta situació i hem pogut anar avançant el treball de forma conjunta.

Al moment de fer el reply dels comments ja que no sabíem molt bé com plantejar el tema de poder fer una reply a un comentari i així fins l’infinit ja que tota reply és un comentari al qual se li pot fer reply. En aquest punt et vam demanar com ho podriem fer i ens vas dir de fer un bfs. Llavors buscant per internet vam veure que hi havien alguns mètodes per poder fer l'arbre de comentaris amb les seves rèpliques i vam seguir el que ens deia el document.

El dia anterior a l’entrega, un cop la web ja estava acabada i només havíem de fer el deployment, ens vam trobar multitud de problemes per a realitzar-ho, fins al punt de començar a les 16:00h i acabar a les 2 del matí. Vam tenir problemes amb la versió de django, amb la versió de python, amb les dependències a requierements.txt, i un llarg etcetera. Al final després de moltes proves i molts problemes vam aconseguir realitzar el deployment,  però ens vam trobar amb alguns problemes visuals a Heroku que no haviem trobat anteriorment, per tant vam haver de fer algunes modificacions.

