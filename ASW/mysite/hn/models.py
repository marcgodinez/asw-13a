from django.db import models
from django.urls import reverse #Used to generate URLs by reversing the URL patterns
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from django.db.models.signals import post_save

from mptt.models import MPTTModel, TreeForeignKey

#from django.conf import settings
#from django.contrib.contenttypes.fields import GenericForeignKey
#from django.contrib.contenttypes.models import ContentType


class Submit(models.Model):

    title = models.CharField(max_length=200)
    url = models.CharField(max_length=100,blank=True)
    text = models.TextField(max_length=400,blank=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date_added = models.DateTimeField('date published', auto_now_add=True)
    likes = models.IntegerField(default=0)
    path = models.CharField(max_length=100,blank=True)
    qttcom = models.IntegerField(default=0)
    def __str__(self):

        return self.title

class Like(models.Model):
    post = models.ForeignKey('Submit', on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date = models.DateTimeField(auto_now= True)

    def str(self):
        return str(self.user) + ':' + str(self.post)

    class Meta:
       unique_together = ("user", "post")

class LikeComment(models.Model):
    post = models.ForeignKey('Comment', on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date = models.DateTimeField(auto_now= True)

    def str(self):
        return str(self.user) + ':' + str(self.post) +':' + str(self.value)

    class Meta:
       unique_together = ("user", "post")

class Comment(MPTTModel):
    submit = models.ForeignKey('Submit', on_delete=models.CASCADE, null=True)
    text = models.TextField(max_length=400)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    date_added = models.DateTimeField('date published', auto_now_add=True)
    likes = models.IntegerField(default=0)
    nivel = models.IntegerField(default=0)
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='children'
    )

    # Added, record secondary comments to whom, str
    reply_to = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='replyers'
    )

    # Replace Meta with MPTTMeta
    # class Meta:
    #     ordering = ('created',)
    class MPTTMeta:
        order_insertion_by = ['date_added']

class Usuari(models.Model):
        user = models.ForeignKey(User, on_delete=models.CASCADE)
        about = models.TextField(max_length=400,blank=True)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Usuari.objects.create(user=instance)
