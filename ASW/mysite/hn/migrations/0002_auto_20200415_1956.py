# Generated by Django 3.0.4 on 2020-04-15 17:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hn', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='level',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='lft',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='rght',
        ),
        migrations.RemoveField(
            model_name='comment',
            name='tree_id',
        ),
    ]
